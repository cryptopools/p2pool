import hashlib

str_test = "This is a Test String"
print "String To Be Hashed: %s \n" % str_test

print "Hash 1 (Non Standard) \n"
print hashlib.sha256(str_test).hexdigest()

print "Hash 2 (Like Normally Done \n"
print hashlib.sha256(hashlib.sha256(str_test).digest()).hexdigest()
